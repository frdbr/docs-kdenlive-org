.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Carl Schwan <carl@carlschwan.eu>

   :license: Creative Commons License SA 4.0

.. _project_menu:

Project Menu
============

.. contents::

.. image:: /images/Kdenlive_Project_Menu.png
  :align: center
  :alt: Project Menu

* :ref:`Add Clip <add_clip>`

* :ref:`Add Color Clip <add_color_clip>`

* :ref:`Add Slideshow Clip <add_slideshow_clip>`

* :ref:`titles`

* :ref:`titles`

* :ref:`create_folder`

* :ref:`online_resources`

* :ref:`generators`

* :ref:`view_mode`

* :ref:`clean_project`

* :ref:`render`

* :ref:`extract_audio`

* :ref:`adjust_profile_to_current_clip`

* :ref:`project_settings`

* :ref:`open_backup_file`

* :ref:`archiving`


.. toctree::
   :caption: Contents:
   :glob:

   project_menu/*
