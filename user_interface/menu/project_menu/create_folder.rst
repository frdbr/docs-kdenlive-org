.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Carl Schwan <carl@carlschwan.eu>

   :license: Creative Commons License SA 4.0

.. _create_folder:

Create Folder
=============

.. contents::




There are three ways to access a menu containing this option: from the :ref:`project_menu` menu, the :ref:`clips` dropdown on the Project Bin toolbar or by right-clicking on a clip in the Project Bin.   This menu item creates a folder in the Project Bin. It is a virtual folder, not one created on your hard disk.  You can use this feature to organize your Project Bin when it gets very large or complex by placing clips in folders, which can then be collapsed to free up space in the tree.  Existing clips in the Project Bin can be moved to a folder using drag and drop.  New clips can be added directly to a folder by first selecting the folder (or any clip in the folder) and then choosing an add clip option from one of the dropdown menus described above.


.. image:: /images/Kdenlive_Create_folder.png
  :align: center


Click on the text **Folder** to the right of the icon to edit the name of the folder.


