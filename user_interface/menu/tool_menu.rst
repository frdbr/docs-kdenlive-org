.. metadata-placeholder

   :authors: - Yuri Chornoivan
             - Jack (https://userbase.kde.org/User:Jack)
             - Carl Schwan <carl@carlschwan.eu>

   :license: Creative Commons License SA 4.0

.. _tool_menu:

Tool Menu
=========

.. contents::

.. image:: /images/Kdenlive_tool_menu_selection_kde.png
  :align: center
  :alt: Tool menu selection


The options on this menu provide two modes and three tools which affect how operations are performed on clips in the timeline. These same options can also be accessed from the :ref:`editing` and more details on their usage can be found there.


