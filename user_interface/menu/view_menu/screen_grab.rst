.. metadata-placeholder

   :authors: - Yuri Chornoivan
             - Eugen Mohr

   :license: Creative Commons License SA 4.0

.. _screen_grab:

Screen Grab
===========

.. contents::




.. image:: /images/Kdenlive_screen-grab.png
   :align: left
   :alt: screen grab
 


Start recording: click the “record” button.  


Stop record: click the "record" button again.  


Clicking on the configure button brings you to the :ref:`configure_kdenlive` window.


