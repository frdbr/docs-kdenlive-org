.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _glossary:

########
Glossary
########

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   glossary/*