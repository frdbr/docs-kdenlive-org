.. metadata-placeholder

   :authors: - Eugen Mohr
             - Julius Künzel <jk.kdedev@smartlab.uber.space

   :license: Creative Commons License SA 4.0


###############
Kdenlive Manual
###############

Welcome to the manual for `kdenlive <https://kdenlive.org>`_, the free and open source video editor.

###############
Getting started
###############

.. container:: toctile

    .. container:: tile no-descr

        :ref:`Introduction`
            
          

    .. container:: tile no-descr

        :ref:`Installation`


    .. container:: tile

        :ref:`quickstart`
           Basic workflow with a video example.

    .. container:: tile no-descr

            :ref:`Tutorials`


##############
User Interface
##############

.. container:: toctile

    .. container:: tile

       :ref:`user_interface`
          Introduction to Kdenlive's window system and widgets

    .. container:: tile

       :ref:`Project_Settings`
          Setting the correct project values

    .. container:: tile

       Key components
          :ref:`Project_tree`

          :ref:`Timeline`

          :ref:`Monitors`

          :ref:`toolbars`

    .. container:: tile

       :ref:`shortcuts`
          Improve your workflow by using the keyboard


########
Workflow
########
  
.. container:: toctile

    .. container:: tile

        .. figure:: /images/intro_page/Index_Logging.png
           :target: importing_and_assets_management.html

        :doc:`importing_and_assets_management`
           Load files into Kdenlive and be prepared

           Starting video editing

    .. container:: tile

        .. figure:: /images/intro_page/Index_Editing.png

        :ref:`Cutting_and_assembling`
           Start editing in the Timeline

           See how the time line and the monitors are working

    .. container:: tile

       .. figure:: /images/intro_page/Index_Effects.png

       :ref:`Effects`
          Add video and audio effects and compositions.

          Create Titles and Subtitles and use Speech to Text.

          Make color correction.

    .. container:: tile

       .. figure:: /images/intro_page/Index_Rendering.png

       :ref:`Exporting`
          Render out your final video for distributing.


#######################################
Troubleshooting, Glossary, Get Involved
#######################################

.. container:: toctile

   .. container:: tile

      :ref:`troubleshooting`
         Solving specific :ref:`windows_issues`

         General problem solving

   .. container:: tile

      `Bug Reports <https://kdenlive.org/en/bug-reports/>`__
         How to file a bug.

   .. container:: tile

      :ref:`Glossary`
         References and further information.


   .. container:: tile

      `Get Involved <https://community.kde.org/Kdenlive/Workgroup/Documentation>`__
         Contribute to this Manual.


.. toctree::
   :maxdepth: 2
   :hidden:
     
   getting_started
   user_interface
   importing_and_assets_management
   cutting_and_assembling
   effects_and_compositions
   exporting
   troubleshooting
   glossary
   get_involved
