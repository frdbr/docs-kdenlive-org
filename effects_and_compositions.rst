.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _effects_and_compositions:

########################
Effects and compositions
########################


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   effects_and_compositions/*