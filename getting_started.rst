.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _getting_started:

###############
Getting started
###############

A short overview to start with Kdenlive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   getting_started/*