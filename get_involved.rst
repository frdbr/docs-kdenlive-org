.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _get_involved:

############
Get involved
############

Contribute to this Manual.
https://community.kde.org/Kdenlive/Workgroup/Documentation
