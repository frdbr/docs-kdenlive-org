.. metadata-placeholder

   :authors: - Eugen Mohr

   :license: Creative Commons License SA 4.0


.. _scopes_directx:

Windows issue with scopes
=========================


.. note::

   Video scopes do not work with DirectX under Windows. 
   
   Workaround: Change the backend to OpenGL (:menuselection:`Settings --> OpenGL Backend --> OpenGL`)
