.. metadata-placeholder

   :authors: - Eugen Mohr
             
   :license: Creative Commons License SA 4.0

.. _cutting_and_assembling:

######################
Cutting and assembling
######################



.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   cutting_and_assembling/*
   
