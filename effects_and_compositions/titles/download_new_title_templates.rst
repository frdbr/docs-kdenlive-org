.. metadata-placeholder

   :authors: - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)
             - Roger (https://userbase.kde.org/User:Roger)
             - Smolyaninov (https://userbase.kde.org/User:Smolyaninov)

   :license: Creative Commons License SA 4.0

.. _download_new_title_templates:

Download New Title Templates
============================

.. contents::

To install more title templates press the :guilabel:`Download New Title Templates...` icon on the tool bar when you are in the title editor.

.. image:: /images/Kdenlive_download_new_title_templates.png
   :align: center
   :alt: Kdenlive_download_new_title_templates

If you have a good title template, you can post it `here <https://store.kde.org/browse/cat/335/>`_ so that other **Kdenlive** users can download it through :menuselection:`Download New Title Templates...` and use it.

.. image:: /images/Kdenlive_Download_title_templates.png
   :align: center
   :alt: Kdenlive_Download_title_templates

Once these title templates are installed, they can be accessed via the :ref:`titles` drop down on the **Title Clip** dialog.

The :file:`.kdenlivetitle` files that are downloaded are installed to :file:`.local/share/kdenlive/titles/`.

