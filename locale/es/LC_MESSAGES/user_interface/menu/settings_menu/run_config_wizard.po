# Spanish translations for docs_kdenlive_org_user_interface___menu___settings_menu___run_config_wizard.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___settings_menu___run_config_wizard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-14 04:57+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:13
msgid "Run Config Wizard"
msgstr ""

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/settings_menu/run_config_wizard.rst:18
msgid ""
"This feature re-runs the config wizard that runs when you first install or "
"upgrade **Kdenlive**. It gives you the opportunity to choose the default "
"settings again for things like the default project settings. It also resets "
"many settings back to \"factory defaults\" so it can be useful to run this "
"if the **Kdenlive** application is misbehaving."
msgstr ""
