msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-30 15:23\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_user_interface___menu___settings_menu___manage_project_profiles."
"pot\n"
"X-Crowdin-File-ID: 26255\n"

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:13
msgid "Manage Project Profiles"
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:16
msgid "Contents"
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:18
msgid "This is available from the **Settings** menu."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:24
msgid "Once the dialog appears, select a profile to modify from the drop down."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:26
msgid ""
"Next, click the button with a green plus on it.  This will make all the "
"*Properties* fields editable."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:28
msgid ""
"Fill in the settings for your project profile, give it a new *Description* "
"and click the **OK** button."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:30
msgid ""
"See also `HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum."
"kde.org/viewtopic.php?f=272&amp;t=124869#p329129>`_"
msgstr ""
