# Translation of docs_kdenlive_org_user_interface___menu___file_menu___dvd_wizard.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-15 20:09+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:16
msgid "DVD Wizard"
msgstr "Assistent de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:21
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:23
msgid "Removed since Version 21.08"
msgstr "Eliminat des de la versió 21.08"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:25
msgid ""
"With the move to MLT7 the DVD Wizard was removed as it was unmaintained "
"since long."
msgstr ""
"Amb el canvi a MLT7 l'assistent de DVD s'eliminarà perquè feia temps que no "
"es mantenia."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:28
msgid ""
"This feature allows you to author a DVD with a simple menu. Nothing flashy."
msgstr ""
"Aquesta característica permet crear un DVD amb un menú senzill. Res "
"espectacular."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:32
msgid "Screen 1 of the DVD Wizard"
msgstr "Pantalla 1 de l'assistent de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:36
msgid ""
"Select a file here that you have rendered using one of the :ref:`render`."
msgstr ""
"Seleccioneu aquí un fitxer que hàgiu renderitzat usant un dels :ref:`render`."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:43
msgid "Screen 2  of the DVD Wizard"
msgstr "Pantalla 2 de l'assistent de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:47
msgid ""
"First click on the **00:00:00** in the center to select a file to play. Then "
"play the file in the preview window and add chapters at the cursor's time "
"point by clicking the :menuselection:`Add Chapter` button."
msgstr ""
"Primer feu clic en el **00:00:00** del centre per a seleccionar un fitxer a "
"reproduir. Després reproduïu el fitxer a la finestra de vista prèvia i "
"afegiu capítols en el punt de temps del cursor fent clic al botó :"
"menuselection:`Afegeix capítol`."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:54
msgid "Screen 3 of the DVD Wizard"
msgstr "Pantalla 3 de l'assistent de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:58
msgid ""
"The first two buttons on the side allow you to add and delete menu buttons. "
"You define what the button does using the **Target** drop down list."
msgstr ""
"Els dos primers botons del lateral permeten afegir i suprimir botons de "
"menú. Definiu què fa el botó usant la llista desplegable **Destí**."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:65
msgid "Screen 4 of the DVD Wizard"
msgstr "Pantalla 4 de l'assistent de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:69
msgid ""
"Click the :menuselection:`Create ISO Image` button on this screen to author "
"your DVD. Click :menuselection:`Finish` only after you have clicked  :"
"menuselection:`Create ISO Image` button. Clicking :menuselection:`Finish` "
"closes the DVD wizard without prompting for you to complete the job."
msgstr ""
"Feu clic al botó :menuselection:`Crea una imatge ISO` en aquesta pantalla "
"per a crear el DVD. Només feu clic a :menuselection:`Finalitza` després "
"d'haver fet clic al botó :menuselection:`Crea una imatge ISO`. En fer clic "
"a :menuselection:`Finalitza` es tanca l'assistent de DVD sense preguntar-vos "
"per a completar el treball."

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:75
msgid "Result of successful DVD creation"
msgstr "Resultat d'una creació exitosa de DVD"

#: ../../user_interface/menu/file_menu/dvd_wizard.rst:81
msgid ""
"Resulting iso-file can then be written into writable DVD-disk using programs "
"like K3b. File can also be viewed with most video player applications like "
"Kaffeine, Vlc or Smplayer (Disk menu might have issues to play correctly)."
msgstr ""
"El fitxer ISO resultant després es pot gravar en un disc DVD gravable usant "
"programes com el K3b. El fitxer també es pot veure amb la majoria "
"d'aplicacions de reproducció de vídeo com el Kaffeine, Vlc o Smplayer (el "
"menú de disc podria tenir problemes per reproduir-se correctament)."
