# Translation of docs_kdenlive_org_user_interface___menu___view_menu___waveform.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-21 14:18+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/view_menu/waveform.rst:13
msgid "Waveform"
msgstr "Forma d'ona"

#: ../../user_interface/menu/view_menu/waveform.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/view_menu/waveform.rst:18
msgid ""
"This data is a 3D histogram.  It represents the Luma component (whiteness) "
"of the video. It is the same type of graph as for the :ref:`rgb_parade`. The "
"horizontal axis represents the horizontal axis in the video frame. The "
"vertical axis is the pixel luma from 0 to 255. The brightness of the point "
"on the graph represents the count of the number of pixels with this luma in "
"this column of pixels in the video frame."
msgstr ""
"Aquestes dades són un histograma 3D. Representa el component de Luma (blanc) "
"del vídeo. És el mateix tipus de gràfic que el de :ref:`rgb_parade`. L'eix "
"horitzontal representa l'eix horitzontal en el fotograma de vídeo. L'eix "
"vertical és la luma dels píxels de 0 a 255. La brillantor del punt en el "
"gràfic representa el nombre de píxels amb aquesta luma en aquesta columna de "
"píxels en el fotograma del vídeo."

# skip-rule: t-acc_obe
#: ../../user_interface/menu/view_menu/waveform.rst:26
msgid ""
"For more information see `Granjow's blog <http://kdenlive.org/users/granjow/"
"introducing-color-scopes-waveform-and-rgb-parade>`_ on the waveform and RGB "
"parade scopes. This blog gives some information on how to use the data "
"provided by the RGB parade to do color correction on video footage."
msgstr ""
"Per a més informació, vegeu el `blog de Granjow <http://kdenlive.org/users/"
"granjow/introducing-color-scopes-waveform-and-rgb-parade>`_ sobre les formes "
"d'ona i els electroscopis de l'histograma RGB. Aquest blog dona informació "
"sobre com utilitzar les dades proporcionades per l'histograma RGB per a fer "
"correccions de color en el metratge de vídeo."

#: ../../user_interface/menu/view_menu/waveform.rst:30
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"
