# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___misc___sigmoidaltransfer.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 07:01+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:11
msgid "sigmoidaltransfer"
msgstr "Transferència sigmoide"

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:13
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:15
msgid ""
"This is the `Frei0r sigmoidaltransfer <https://www.mltframework.org/plugins/"
"FilterFrei0r-sigmoidaltransfer/>`_ MLT filter by Janne Liljeblad."
msgstr ""
"Aquest és el `filtre «sigmoidaltransfer» de Frei0r <https://www.mltframework."
"org/plugins/FilterFrei0r-sigmoidaltransfer/>`_ del MLT, per en Janne "
"Liljeblad."

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:17
msgid ""
"Desaturates image and creates a particular look that could be called Stamp, "
"Newspaper or Photocopy."
msgstr ""
"Dessatura la imatge i crea un aspecte particular que es pot anomenar segell, "
"diari o fotocòpia."

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:19
msgid "Two parameters:"
msgstr "Dos paràmetres:"

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:21
msgid "**Brightness**: Controls Brightness of image. Range 0 to 1."
msgstr ""
"**Brillantor**: Controla la brillantor de la imatge. En l'interval de 0 a 1."

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:23
msgid "**Sharpness**: Controls sharpness of transfer. Range 0 to 1."
msgstr ""
"**Agudesa**: Controla l'agudesa de la transferència. En l'interval de 0 a 1."

#: ../../effects_and_compositions/effect_groups/misc/sigmoidaltransfer.rst:25
msgid ""
"Both parameters default to 0 in **Kdenlive** - which is unfortunate because "
"this results in a totally black frame. You need to have numbers above zero "
"to see the effect."
msgstr ""
"Ambdós paràmetres són 0 de manera predeterminada en el **Kdenlive**, la qual "
"cosa és lamentable perquè resulta en un fotograma totalment negre. Per a "
"veure l'efecte, cal tenir números per sobre de zero."
