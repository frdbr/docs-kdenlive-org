# Translation of docs_kdenlive_org_importing_and_assets_management___projects_and_files___project_settings.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-28 12:18+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:17
msgid "Project Settings Dialog"
msgstr "Diàleg de configuració del projecte"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:20
msgid "Contents"
msgstr "Contingut"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:22
msgid ""
"This is reached via  :menuselection:`Project Settings` in the :ref:"
"`project_menu` menu. This dialog has 3 Tabs."
msgstr ""
"Aquí s'arriba a través de :menuselection:`Configuració del projecte` al "
"menú :ref:`project_menu`. Aquest diàleg té 3 pestanyes."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:26
msgid "Project Settings Tab"
msgstr "Pestanya configuració del projecte"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:33
msgid ""
"The Project Settings dialog is shown when you start a new project (:"
"menuselection:`File --> New`). This allows you to set all basic properties "
"for your project. You can also edit the properties of your current :ref:"
"`project_menu` in :menuselection:`Project --> Project Settings`."
msgstr ""
"El diàleg Arranjament del projecte es mostra quan inicieu un projecte nou (:"
"menuselection:`Fitxer --> Nou`). Això us permet establir totes les "
"propietats bàsiques del projecte. També podeu editar les propietats del :ref:"
"`project_menu` actual a :menuselection:`Projecte --> Configuració del "
"projecte`."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:38
msgid "Project Folder"
msgstr "Carpeta del projecte"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:40
msgid ""
"As recommended in the :ref:`quickstart` section, you should create a new "
"folder for your project. This folder will hold all temporary files that are "
"used during the editing of your project (thumbnails, proxy clips, etc)."
msgstr ""
"Com es recomana a la secció :ref:`quickstart`, s'hauria de crear una carpeta "
"nova per al vostre projecte. Aquesta carpeta conté tots els fitxers "
"temporals que s'utilitzen durant l'edició del projecte (miniatures, clips "
"intermediaris, etc.)."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:44
msgid "Video Profile"
msgstr "Perfil de vídeo"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:46
msgid ""
"The video profile will define the format of your project. A list of "
"predefined formats is available in **Kdenlive**, for example *DV / DVD PAL*, "
"*HD 1080i 25 fps*, etc."
msgstr ""
"El perfil de vídeo definirà el format del projecte. Hi ha disponible una "
"llista de formats predefinits al **Kdenlive**, per exemple *DV / DVD PAL*, "
"*HD 1080i 25 fps*, etc."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:49
msgid ""
"You can use the pull-down menus to filter the list of profiles by FPS "
"(Frames per second) or Scanning (Interlaced or Progressive)"
msgstr ""
"Podeu utilitzar els menús desplegables per a filtrar la llista de perfils "
"per FPS (fotogrames per segon) o Escaneig (entrellaçat o progressiu)"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:52
msgid ""
"The profile defines the video resolution, as well as display aspect ratio, "
"color space and a few other parameters."
msgstr ""
"El perfil defineix la resolució de vídeo, així com la relació d'aspecte de "
"la pantalla, l'espai de color i alguns altres paràmetres."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:55
msgid ""
"You should carefully choose your project format and select the one which "
"best fits your desired output. All video operations on the project (like "
"compositing, scaling, etc) will then use this profile. Advanced users can "
"create custom project profiles in  :ref:`manage_project_profiles`."
msgstr ""
"Hauríeu d'escollir amb cura el format del projecte i seleccionar el que "
"millor s'ajusti a la sortida desitjada. Totes les operacions de vídeo en el "
"projecte (com la composició, l'escalat, etc) utilitzaran aquest perfil. Els "
"usuaris avançats poden crear perfils de projecte personalitzats a :ref:"
"`manage_project_profiles`."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:58
msgid ""
"For example, if your goal is to create a DVD, you should use a DVD profile "
"with the correct frame rate (PAL / NTSC) and display ratio (widescreen or "
"not)."
msgstr ""
"Per exemple, si l'objectiu és crear un DVD, hauries d'utilitzar un perfil de "
"DVD amb la velocitat de fotogrames correcta (PAL / NTSC) i la relació de "
"visualització (pantalla ampla o no)."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:62
msgid "Tracks"
msgstr "Pistes"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:64
msgid ""
"You can select the default number of audio and video tracks that your "
"project will have. You can always add or remove tracks in an existing "
"project."
msgstr ""
"Podeu seleccionar el nombre predeterminat de pistes d'àudio i vídeo que "
"tindrà el vostre projecte. Sempre es poden afegir o eliminar pistes en un "
"projecte existent."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:68
msgid "Thumbnails"
msgstr "Miniatures"

# skip-rule: kct-statusbar
#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:70
msgid ""
"The Audio and Video thumbnails are shown in the :ref:`timeline`. They can "
"also be enabled/disabled through buttons in the :ref:`status_bar`."
msgstr ""
"Les miniatures d'àudio i vídeo es mostren a la :ref:`timeline`. També es "
"poden activar/desactivar a través de botons a la :ref:`status_bar`."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:74
msgid "Proxy Clips Tab"
msgstr "Pestanya de clips intermediaris"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:81
msgid ""
"When the :menuselection:`Proxy Clip` feature is enabled, **Kdenlive** will "
"automatically create reduced versions of your source clips, and use these "
"versions for your editing. **Kdenlive** will replace the proxy clips with "
"the originals for a full resolution when rendering."
msgstr ""
"Quan la característica :menuselection:`Clip intermediari` està activada, el "
"**Kdenlive** crearà automàticament versions reduïdes dels vostres clips "
"d'origen, i usarà aquestes versions per a l'edició. El **Kdenlive** "
"substituirà els clips intermediaris pels originals per a una resolució "
"completa en renderitzar."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:83
msgid ""
"The :menuselection:`Generate for videos larger than x pixels` option will "
"automatically create proxy clips for all videos added to the project that "
"have a frame width larger than x. This also applies to images."
msgstr ""
"L'opció :menuselection:`Genera'n per als vídeos més grans que x píxels` "
"crearà automàticament clips intermediaris per a tots els vídeos afegits al "
"projecte que tinguin una amplada de fotograma més gran que x. Això també "
"s'aplica a les imatges."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:85
msgid ""
"You also have the choice to manually enable / disable proxy clips for each "
"clip in your project bin by right-clicking on the clip and choosing :"
"menuselection:`Proxy Clip`."
msgstr ""
"També teniu l'opció d'activar / desactivar manualment els clips "
"intermediaris per a cada clip en la safata del projecte fent clic dret sobre "
"el clip i escollint :menuselection:`Clip intermediari`."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:87
msgid ""
"You can choose an *Encoding profile* for the proxy clips, which will define "
"the size, codecs and bitrate used when creating a proxy. The proxy profiles "
"can be managed from the **Kdenlive** Settings dialog (:menuselection:"
"`Settings --> Configure Kdenlive --> Project Defaults`)."
msgstr ""
"Podeu triar un *Perfil de codificació* per als clips intermediaris, que "
"definirà la mida, els còdecs i la taxa de bits utilitzats en crear un "
"intermediari. Els perfils dels intermediaris es poden gestionar des del "
"diàleg de configuració del **Kdenlive** (:menuselection:`Arranjament --> "
"Configura el Kdenlive --> Valors predeterminats del projecte`)."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:91
msgid "Metadata Tab"
msgstr "Pestanya de metadades"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:93
msgid "Screenshots below show the **Metadata** tab of **Kdenlive**."
msgstr ""
"Les captures de pantalla següents mostren la pestanya **Metadades** del "
"**Kdenlive**."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:96
msgid ""
"Metadata set up here will be written to the files rendered from the project "
"if :ref:`render` is checked in File Rendering."
msgstr ""
"Les metadades definides aquí s'escriuran als fitxers renderitzats des del "
"projecte si :ref:`render` està activat en la renderització de fitxers."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:102
msgid "Project Files Tab"
msgstr "Pestanya fitxers del projecte"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:108
msgid "From here you can export the project files data."
msgstr "Des d'aquí podeu exportar les dades dels fitxers del projecte."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:111
msgid ""
"If you want to remove unused files from your project use Project >  :ref:"
"`clean_project`."
msgstr ""
"Si voleu eliminar els fitxers no utilitzats del vostre projecte utilitzeu "
"Projecte > :ref:`clean_project`."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:115
msgid "Cache Data Tab"
msgstr "Pestanya de dades de la memòria cau"

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:122
msgid ""
"The Cache data tab shows the data used in the project including the timeline "
"preview, proxy clips, audio thumbnails, and video thumbnails."
msgstr ""
"La pestanya de dades de la memòria cau mostra les dades utilitzades en el "
"projecte incloent la previsualització de la línia de temps, els clips "
"intermediaris, les miniatures d'àudio i les miniatures de vídeo."

#: ../../importing_and_assets_management/projects_and_files/project_settings.rst:125
msgid ""
"You can click on the trashcan icon to clear the cache data for that category."
msgstr ""
"Podeu fer clic a la icona paperera per esborrar les dades de la memòria cau "
"d'aquesta categoria."
