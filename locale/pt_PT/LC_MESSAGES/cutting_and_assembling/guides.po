# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-13 11:26+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../cutting_and_assembling/guides.rst:20
msgid "Guides"
msgstr "Guias"

#: ../../cutting_and_assembling/guides.rst:25
msgid "Contents"
msgstr "Conteúdo"

#: ../../cutting_and_assembling/guides.rst:31
msgid ""
"Guides are labels on the timeline that can be added by right-clicking at a "
"spot on the timeline scale and choosing :menuselection:`Add Guide`. You can "
"put a comment in the guide and make the comment display by choosing :ref:"
"`editing` in the :menuselection:`Timeline` menu or by clicking on the :ref:"
"`editing` button."
msgstr ""

#: ../../cutting_and_assembling/guides.rst:34
msgid ""
"Guides in the pic below are the purple flags. Not to be confused with :ref:"
"`clips` (gold in the picture below). Guides are static on the timeline and "
"are stationary when clips are moved around. Markers are inside the clips and "
"move with the clips."
msgstr ""

#: ../../cutting_and_assembling/guides.rst:42
msgid ""
"Guides can be used to define regions for rendering. See :ref:`rendering`."
msgstr ""

#: ../../cutting_and_assembling/guides.rst:45
msgid ""
"Guides can also be used as chapters for DVD videos. See :ref:`rendering`."
msgstr ""

#: ../../cutting_and_assembling/guides.rst:49
msgid "Move Guides with Spacer Tool"
msgstr ""

#: ../../cutting_and_assembling/guides.rst:53
msgid ""
"Easily moves Guides along with clips using the Spacer Tool by using the new :"
"menuselection:`Guides Locked` option."
msgstr ""
