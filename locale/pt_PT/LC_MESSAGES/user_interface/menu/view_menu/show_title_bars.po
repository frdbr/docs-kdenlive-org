# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-22 00:47+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Kdenlive\n"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:15
msgid "Show Title Bars"
msgstr "Mostrar as Barras do Título"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:17
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:19
msgid ""
"This toggles the display of the title bar and control buttons on dockable "
"windows in **Kdenlive**."
msgstr ""
"Isto activa/desactiva a apresentação da barra de título e dos botões de "
"controlo nas janelas acopláveis do **Kdenlive**."
