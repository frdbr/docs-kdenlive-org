# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-17 11:54+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../user_interface/menu/view_menu/spectogram.rst:14
msgid "Spectrogram"
msgstr "Spektrogram"

#: ../../user_interface/menu/view_menu/spectogram.rst:17
msgid "Contents"
msgstr "Obsah"

#: ../../user_interface/menu/view_menu/spectogram.rst:19
msgid ""
"This allows you to monitor the audio properties of your clip in detail. The "
"spectrogram displays the loudness (in decibels) of the audio at different "
"audio frequencies over the entire length of the clip.  In the spectrogram, "
"the horizontal axis represents the audio frequency and the loudness is "
"represented by the brightness (version <= 0.9.8) or the colour (version >= "
"0.9.10) of the pixel on the graph. The vertical axis represents frame number."
msgstr ""

#: ../../user_interface/menu/view_menu/spectogram.rst:34
msgid ""
"For more information see `Granjow's blog <http://kdenlive.org/users/granjow/"
"introducing-scopes-audio-spectrum-and-spectrogram>`_ on Spectrogram"
msgstr ""

#: ../../user_interface/menu/view_menu/spectogram.rst:38
msgid ":ref:`scopes_directx`"
msgstr ""
