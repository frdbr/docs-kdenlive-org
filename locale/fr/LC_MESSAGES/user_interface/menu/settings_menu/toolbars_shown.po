# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-24 09:35+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../user_interface/menu/settings_menu/toolbars_shown.rst:13
msgid "Toolbars Shown"
msgstr "Affichage des barres d'outils"

#: ../../user_interface/menu/settings_menu/toolbars_shown.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/settings_menu/toolbars_shown.rst:18
msgid "Toggles the display of the Main and Extra :ref:`toolbars`"
msgstr ""
"Bascule l'affichage des :ref:`barres d'outils` principales ou "
"supplémentaires."
