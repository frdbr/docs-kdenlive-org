msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-11-15 18:32+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../effects_and_compositions/transitions/alphaatop.rst:11
msgid "alphaatop transition"
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:13
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:15
msgid ""
"This is the `Frei0r alphaatop <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaatop>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:17
msgid "The alpha ATOP operation."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:23
msgid "alphaatop is the transition in between."
msgstr ""
